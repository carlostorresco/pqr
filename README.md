# Landing PQR - Chevyplan

### Instalación - Configuracion

Estructura de proyecto basada en Webpack y Gulp como gestores de tareas.

Tener instalado [Node.js](https://nodejs.org/)

Descargar y clonar el proyecto. Seguir los siguientes pasos:

Instalar las dependencias y ejecutar con Gulp para transpilado y compilación:

```sh
$ npm install
$ gulp
```

Los archivos distribuibles y minificados se alojaran en la carpeta outputs(scripts o styles)