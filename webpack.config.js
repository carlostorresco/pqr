const path = require('path');
const uglifyjs = require('uglifyjs-webpack-plugin');
const webpack = require('webpack');

module.exports = {
	entry: { // se pueden especificar varios entry points
    "chevy-pqr" : "./src/scripts/combos/chevy-pqr.js",
	},
	output: { // solo se puede especificar una carpeta output
		filename: '[name].bundle.min.js',
		path: path.resolve(__dirname, 'src/scripts/outputs')
	},
	devtool: "source-map",
	watch: true,
  plugins: [
      new uglifyjs({
          sourceMap: true,
          extractComments: true
      })
	]
};
