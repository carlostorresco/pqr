var $ = require("jquery");
var validate = require("jquery-validation");
var selectize = require("../outputs/selectize.min.js");

var funcionalidadesHome = function () {

	var init = function () {
		getLocations();
		setPlaceHolder()
		fnValidateForm();
		fnCloseModal();
		$('#cboSubject').selectize();
	};

	var getLocations = function(){

		var jsonLocations,
			selectDepto,
			selectCity,
			dropdownDepto,
			dropdownCity;

		$.ajax({ 
			type: 'GET', 
			url: 'scripts/outputs/locations/locations-CO.json', 
			dataType: 'json',
			async: false,
			success: function (data) {
				jsonLocations = data;
				$.each(jsonLocations, function(index, element) {
					$('#cboDepto').append('<option value="'+element.id+'">'+element.departamento+'</option>');
				});
			},
			error: function(requestObject, error, errorThrown){
				console.warn('Error locations-CO', errorThrown);
			}
		});

		dropdownDepto = $('#cboDepto').selectize({
			onChange: function(value) {
				if (!value.length) return;
				selectCity.disable();
				selectCity.clearOptions();
				selectCity.load(function(callback){
					selectCity.enable();
					callback(searchTransformObject(jsonLocations,value));
				})
			}
		});

		dropdownCity = $('#cboCiudad').selectize({
			valueField: 'ciudad',
			labelField: 'ciudad',
			searchField: ['ciudad']
		});

		selectDepto	= dropdownDepto[0].selectize;
		selectCity 	= dropdownCity[0].selectize;
		
	}

	var searchTransformObject = function(jsonLocations, idDepto){
		var objCity = [];
		var aryCity = $.grep(jsonLocations, function( element ) {
			return element.id == idDepto;
		});
		
		$.each(aryCity[0].ciudades, function(index, element) {
			objCity.push({'ciudad': element});
		});

		return objCity;
	}

	var setPlaceHolder = function(){
		var inputItem = $(".label-placeholder");
		inputItem.length && inputItem.each(function() {
			var self = $(this),
				input = self.find(".form-control"),
				placeholderTxt = input.attr("placeholder"),
				placeholder;
			
			input.after('<span class="placeholder">' + placeholderTxt + "</span>"),
			input.attr("placeholder", ""),
			placeholder = self.find(".placeholder"),
			
			input.val().length ? self.addClass("active") : self.removeClass("active"),
				
			input.on("focusout", function() {
				input.val().length ? self.addClass("active") : self.removeClass("active");
			}).on("focus", function() {
				self.addClass("active");
			});
		});
	}

	var fnValidateForm = function(){
		$.validator.addMethod("lettersonly", function(value, element) {
			return this.optional(element) || /^[a-z\s]+$/i.test(value);
		}, "No se permiten caracteres especiales");

		$("#frmPQR").validate({
			rules:{
				'txtFName':{
					required	: true,
					minlength   : 3,
					lettersonly : true
				},
				'txtLName':{
					required	: true,
					minlength   : 3,
					lettersonly : true
				},
				'txtDNI':{
					required	: true,
					number		: true,
					minlength   : 6,
					maxlength	: 10
				},
				'cboCiudad':{
					required	: true
				},
				'txtCelular':{
					required	: true,
					number		: true,
					minlength   : 7,
					maxlength	: 10
				},
				'txtEmail':{
					required	: true,
					email 		: true
				},
				'cboSubject':{
					required	: true
				},
				'txtMessage':{
					required	: true
				}
			},
			messages:{
				'txtFName':{
					required	: 'Tus nombres son requeridos',
					minlength   : 'El campo tiene un mínimo de 3 caracteres',
					lettersonly : 'No se permiten caracteres especiales'
				},
				'txtLName':{
					required	: 'Tus nombres son requeridos',
					minlength   : 'El campo tiene un mínimo de 3 caracteres',
					lettersonly : 'No se permiten caracteres especiales'
				},
				'txtDNI':{
					required	: 'Tu número de identificación es requerida',
					number		: 'Ingresa unicamente números',
					minlength   : 'Ingrese un número de identificación valido',
					maxlength	: 'Ingrese un número de identificación valido'
				},
				'cboCiudad':{
					required	: 'Selecciona una ciudad'
				},
				'txtCelular':{
					required	: 'Tu número de fijo o celular es requerido',
					number		: 'Ingresa unicamente números',
					minlength   : 'Ingrese un número de fijo o celular valido',
					maxlength	: 'Ingrese un número de fijo o celular valido'
				},
				'txtEmail':{
					required	: 'Tu correo electrónico es requerido',
					email 		: 'El formato del email debe ser ejemplo@ejemplo.com'
				},
				'cboSubject':{
					required	: 'Debes seleccionar un asunto'
				},
				'txtMessage':{
					required	: 'Debes indicarnos la descripción del caso'
				}
			},
			errorElement: "label",
			submitHandler: function(form) {
				$('#frmPQR input[type=submit]').addClass('disabled');
				//$(form)[0].submit();
				//return false;
				$('.overlay-PQR').find('.radicado').text('W'+ $('#txtDNI').val());
				$('.overlay-PQR').fadeIn(function(){
					$('.overlay-PQR').find('.modal-body').addClass('show-modal');
				});
			}
		});
	}

	var fnCloseModal = function(){
		$('.overlay-PQR .close, .overlay-PQR button').click(function() {
			$('.overlay-PQR').find('.radicado').text('');
			$('.overlay-PQR').find('.modal-body').removeClass('show-modal');
			$('.overlay-PQR').fadeOut()
		})
	}

	return {init : init};

}();

$(document).ready(function () {
	funcionalidadesHome.init();
});

// Expone públicamente partes del módulo.
module.exports.funcionalidadesHome = funcionalidadesHome;